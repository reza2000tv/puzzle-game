﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball_trigger : MonoBehaviour {
    // Start is called before the first frame update
    void Start () {

    }

    // Update is called once per frame
    void Update () {

    }

    void OnTriggerEnter2D (Collider2D col) {

        GameObject ball = GameObject.Find ("Ball");
        Ball_Movement bm = ball.GetComponent<Ball_Movement> ();

        if (col.gameObject.tag == ("Red Obstacle Left") && bm.have_Red_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Green Obstacle Left") && bm.have_green_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Yellow Obstacle Left") && bm.have_Yellow_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Pink Obstacle Left") && bm.have_Pink_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Purple Obstacle Left") && bm.have_Purple_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Brown Obstacle Left") && bm.have_Brown_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Red Obstacle Right") && bm.have_Red_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Green Obstacle Right") && bm.have_green_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Yellow Obstacle Right") && bm.have_Yellow_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Pink Obstacle Right") && bm.have_Pink_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Purple Obstacle Right") && bm.have_Purple_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Brown Obstacle Right") && bm.have_Brown_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }
    }

    private void OnTriggerStay2D (Collider2D col) {

        GameObject ball = GameObject.Find ("Ball");
        Ball_Movement bm = ball.GetComponent<Ball_Movement> ();

        if (col.gameObject.tag == ("Obstacle Left")) {

            bm.can_move_right = false;
        }

        if (col.gameObject.tag == ("Obstacle Right")) {

            bm.can_move_left = false;
        }

        if (col.gameObject.tag == ("Green Obstacle Left")) {

            bm.can_move_right = false;
        }

        if (col.gameObject.tag == ("Green Obstacle Right")) {

            bm.can_move_left = false;
        }

        if (col.gameObject.tag == ("Red Obstacle Left")) {

            bm.can_move_right = false;
        }

        if (col.gameObject.tag == ("Red Obstacle Right")) {

            bm.can_move_left = false;
        }

        if (col.gameObject.tag == ("Yellow Obstacle Left")) {

            bm.can_move_right = false;
        }

        if (col.gameObject.tag == ("Yellow Obstacle Right")) {

            bm.can_move_left = false;
        }

        if (col.gameObject.tag == ("Pink Obstacle Left")) {

            bm.can_move_right = false;
        }

        if (col.gameObject.tag == ("Pink Obstacle Right")) {

            bm.can_move_left = false;
        }

        if (col.gameObject.tag == ("Purple Obstacle Left")) {

            bm.can_move_right = false;
        }

        if (col.gameObject.tag == ("Purple Obstacle Right")) {

            bm.can_move_left = false;
        }

        if (col.gameObject.tag == ("Brown Obstacle Left")) {

            bm.can_move_right = false;
        }

        if (col.gameObject.tag == ("Brown Obstacle Right")) {

            bm.can_move_left = false;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Up Left")) {

            bm.can_move_right = false;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Up Right")) {

            bm.can_move_left = false;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Down Left")) {

            bm.can_move_right = false;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Down Right")) {

            bm.can_move_left = false;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Right Left")) {

            bm.can_move_right = false;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Right Right")) {

            Destroy (transform.parent.gameObject);
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Left Left")) {

            Destroy (transform.parent.gameObject);
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Left Right")) {

            bm.can_move_left = false;
        }

        if (col.gameObject.tag == ("Green Bomb Left")) {

            bm.GreenBall ();

            bm.can_move_right = false;

            bm.have_Red_bomb = false;
            bm.have_green_bomb = true;
            bm.have_Yellow_bomb = false;
            bm.have_Pink_bomb = false;
            bm.have_Purple_bomb = false;
            bm.have_Brown_bomb = false;
        }

        if (col.gameObject.tag == ("Green Bomb Right")) {

            bm.GreenBall ();

            bm.can_move_left = false;

            bm.have_Red_bomb = false;
            bm.have_green_bomb = true;
            bm.have_Yellow_bomb = false;
            bm.have_Pink_bomb = false;
            bm.have_Purple_bomb = false;
            bm.have_Brown_bomb = false;
        }

        if (col.gameObject.tag == ("Red Bomb Left")) {

            bm.RedBall ();

            bm.can_move_right = false;

            bm.have_Red_bomb = true;
            bm.have_green_bomb = false;
            bm.have_Yellow_bomb = false;
            bm.have_Pink_bomb = false;
            bm.have_Purple_bomb = false;
            bm.have_Brown_bomb = false;
        }

        if (col.gameObject.tag == ("Red Bomb Right")) {

            bm.RedBall ();

            bm.can_move_left = false;

            bm.have_Red_bomb = true;
            bm.have_green_bomb = false;
            bm.have_Yellow_bomb = false;
            bm.have_Pink_bomb = false;
            bm.have_Purple_bomb = false;
            bm.have_Brown_bomb = false;
        }

        if (col.gameObject.tag == ("Yellow Bomb Left")) {

            bm.YellowBall ();

            bm.can_move_right = false;

            bm.have_Red_bomb = false;
            bm.have_green_bomb = false;
            bm.have_Yellow_bomb = true;
            bm.have_Pink_bomb = false;
            bm.have_Purple_bomb = false;
            bm.have_Brown_bomb = false;
        }

        if (col.gameObject.tag == ("Yellow Bomb Right")) {

            bm.YellowBall ();

            bm.can_move_left = false;

            bm.have_Red_bomb = false;
            bm.have_green_bomb = false;
            bm.have_Yellow_bomb = true;
            bm.have_Pink_bomb = false;
            bm.have_Purple_bomb = false;
            bm.have_Brown_bomb = false;
        }

        if (col.gameObject.tag == ("Pink Bomb Left")) {

            bm.PinkBall ();

            bm.can_move_right = false;

            bm.have_Red_bomb = false;
            bm.have_green_bomb = false;
            bm.have_Yellow_bomb = false;
            bm.have_Pink_bomb = true;
            bm.have_Purple_bomb = false;
            bm.have_Brown_bomb = false;
        }

        if (col.gameObject.tag == ("Pink Bomb Right")) {

            bm.PinkBall ();

            bm.can_move_left = false;

            bm.have_Red_bomb = false;
            bm.have_green_bomb = false;
            bm.have_Yellow_bomb = false;
            bm.have_Pink_bomb = true;
            bm.have_Purple_bomb = false;
            bm.have_Brown_bomb = false;
        }

        if (col.gameObject.tag == ("Purple Bomb Left")) {

            bm.PurpleBall ();

            bm.can_move_right = false;

            bm.have_Red_bomb = false;
            bm.have_green_bomb = false;
            bm.have_Yellow_bomb = false;
            bm.have_Pink_bomb = false;
            bm.have_Purple_bomb = true;
            bm.have_Brown_bomb = false;
        }

        if (col.gameObject.tag == ("Purple Bomb Right")) {

            bm.PurpleBall ();

            bm.can_move_left = false;

            bm.have_Red_bomb = false;
            bm.have_green_bomb = false;
            bm.have_Yellow_bomb = false;
            bm.have_Pink_bomb = false;
            bm.have_Purple_bomb = true;
            bm.have_Brown_bomb = false;
        }

        if (col.gameObject.tag == ("Brown Bomb Left")) {

            bm.BrownBall ();

            bm.can_move_right = false;

            bm.have_Red_bomb = false;
            bm.have_green_bomb = false;
            bm.have_Yellow_bomb = false;
            bm.have_Pink_bomb = false;
            bm.have_Purple_bomb = false;
            bm.have_Brown_bomb = true;
        }

        if (col.gameObject.tag == ("Brown Bomb Right")) {

            bm.BrownBall ();

            bm.can_move_left = false;

            bm.have_Red_bomb = false;
            bm.have_green_bomb = false;
            bm.have_Yellow_bomb = false;
            bm.have_Pink_bomb = false;
            bm.have_Purple_bomb = false;
            bm.have_Brown_bomb = true;
        }

    }

    private void OnTriggerExit2D (Collider2D col) {

        GameObject ball = GameObject.Find ("Ball");
        Ball_Movement bm = ball.GetComponent<Ball_Movement> ();

        if (col.gameObject.tag == ("Obstacle Left")) {

            bm.can_move_right = true;
        }

        if (col.gameObject.tag == ("Obstacle Right")) {

            bm.can_move_left = true;
        }

        if (col.gameObject.tag == ("Green Obstacle Left")) {

            bm.can_move_right = true;
        }

        if (col.gameObject.tag == ("Green Obstacle Right")) {

            bm.can_move_left = true;
        }

        if (col.gameObject.tag == ("Red Obstacle Left")) {

            bm.can_move_right = true;
        }

        if (col.gameObject.tag == ("Red Obstacle Right")) {

            bm.can_move_left = true;
        }

        if (col.gameObject.tag == ("Yellow Obstacle Left")) {

            bm.can_move_right = true;
        }

        if (col.gameObject.tag == ("Yellow Obstacle Right")) {

            bm.can_move_left = true;
        }

        if (col.gameObject.tag == ("Pink Obstacle Left")) {

            bm.can_move_right = true;
        }

        if (col.gameObject.tag == ("Pink Obstacle Right")) {

            bm.can_move_left = true;
        }

        if (col.gameObject.tag == ("Purple Obstacle Left")) {

            bm.can_move_right = true;
        }

        if (col.gameObject.tag == ("Purple Obstacle Right")) {

            bm.can_move_left = true;
        }

        if (col.gameObject.tag == ("Brown Obstacle Left")) {

            bm.can_move_right = true;
        }

        if (col.gameObject.tag == ("Brown Obstacle Right")) {

            bm.can_move_left = true;
        }

        if (col.gameObject.tag == ("Green Bomb Left")) {

            bm.can_move_right = true;
        }

        if (col.gameObject.tag == ("Green Bomb Right")) {

            bm.can_move_left = true;
        }

        if (col.gameObject.tag == ("Red Bomb Left")) {

            bm.can_move_right = true;
        }

        if (col.gameObject.tag == ("Red Bomb Right")) {

            bm.can_move_left = true;
        }

        if (col.gameObject.tag == ("Yellow Bomb Left")) {

            bm.can_move_right = true;
        }

        if (col.gameObject.tag == ("Yellow Bomb Right")) {

            bm.can_move_left = true;
        }

        if (col.gameObject.tag == ("Pink Bomb Left")) {

            bm.can_move_right = true;
        }

        if (col.gameObject.tag == ("Pink Bomb Right")) {

            bm.can_move_left = true;
        }

        if (col.gameObject.tag == ("Purple Bomb Left")) {

            bm.can_move_right = true;
        }

        if (col.gameObject.tag == ("Purple Bomb Right")) {

            bm.can_move_left = true;
        }

        if (col.gameObject.tag == ("Brown Bomb Left")) {

            bm.can_move_right = true;
        }

        if (col.gameObject.tag == ("Brown Bomb Right")) {

            bm.can_move_left = true;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Up Left")) {

            bm.can_move_right = true;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Up Right")) {

            bm.can_move_left = true;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Down Left")) {

            bm.can_move_right = true;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Down Right")) {

            bm.can_move_left = true;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Left Right")) {

            bm.can_move_left = true;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Left Left")) {

            bm.can_move_right = true;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Right Right")) {

            bm.can_move_left = true;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Right Left")) {

            bm.can_move_right = true;
        }
    }

}