﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class New_Ball : MonoBehaviour {

    private Rigidbody2D RB;

    public bool go_up = false;

    public bool go_down = true;

    public float speed;

    public bool have_Pink_bomb = false;

    public bool have_Purple_bomb = false;

    public bool have_Brown_bomb = false;

    public bool have_Red_bomb = false;

    public bool have_green_bomb = false;

    public bool have_Yellow_bomb = false;

    public float up_and_down_speed = 3;

    public float horizontalMovement;

    public bool can_move_left, can_move_right;
    // Start is called before the first frame update
    void Start () {

        speed = 0.03f;

        can_move_left = true;
        can_move_right = true;

    }

    // Update is called once per frame
    void Update () {

        Rigidbody2D RB = GetComponent<Rigidbody2D> ();

        horizontalMovement = Input.GetAxis ("Horizontal");

        if (can_move_left == false && can_move_right == true) {
            if (horizontalMovement > 0) {
                transform.position = new Vector2 (transform.position.x + horizontalMovement * speed, transform.position.y);
            }
        }

        if (can_move_left == true && can_move_right == false) {
            if (horizontalMovement < 0) {
                transform.position = new Vector2 (transform.position.x + horizontalMovement * speed, transform.position.y);
            }
        }
        if (can_move_left == true && can_move_right == true) {

            transform.position = new Vector2 (transform.position.x + horizontalMovement * speed, transform.position.y);
        }

        if (go_down == true && go_up == false) {

            RB.velocity = new Vector2 (0, -up_and_down_speed);
        }

        if (go_down == false && go_up == true) {

            RB.velocity = new Vector2 (0, up_and_down_speed);
        }

    }

    void OnCollisionEnter2D (Collision2D col) {

        if (col.gameObject.tag == ("Death")) {

            SceneManager.LoadScene ("Level1");

        }

        if (col.gameObject.tag == ("Obstacle Up")) {

            go_down = false;
            go_up = true;

        }

        if (col.gameObject.tag == ("Obstacle Down")) {

            go_down = true;
            go_up = false;

        }

        if (col.gameObject.tag == ("Red Obstacle Up")) {

            go_down = false;
            go_up = true;

        }

        if (col.gameObject.tag == ("Red Obstacle Down")) {

            go_down = true;
            go_up = false;

        }

        if (col.gameObject.tag == ("Green Obstacle Up")) {

            go_down = false;
            go_up = true;

        }

        if (col.gameObject.tag == ("Green Obstacle Down")) {

            go_down = true;
            go_up = false;

        }

        if (col.gameObject.tag == ("Yellow Obstacle Up")) {

            go_down = false;
            go_up = true;

        }

        if (col.gameObject.tag == ("Yellow Obstacle Down")) {

            go_down = true;
            go_up = false;

        }

        if (col.gameObject.tag == ("Pink Obstacle Up")) {

            go_down = false;
            go_up = true;

        }

        if (col.gameObject.tag == ("Pink Obstacle Down")) {

            go_down = true;
            go_up = false;

        }

        if (col.gameObject.tag == ("Purple Obstacle Up")) {

            go_down = false;
            go_up = true;

        }

        if (col.gameObject.tag == ("Purple Obstacle Down")) {

            go_down = true;
            go_up = false;

        }

        if (col.gameObject.tag == ("Brown Obstacle Up")) {

            go_down = false;
            go_up = true;

        }

        if (col.gameObject.tag == ("Brown Obstacle Down")) {

            go_down = true;
            go_up = false;

        }

        if (col.gameObject.tag.Equals ("Flag")) {

            SceneManager.LoadScene ("Level1");

        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Up Up")) {

            Destroy (gameObject);

        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Up Down")) {

            go_down = true;
            go_up = false;

        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Down Up")) {

            go_down = false;
            go_up = true;

        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Down Down")) {

            Destroy (gameObject);

        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Left Up")) {

            go_down = false;
            go_up = true;

        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Left Down")) {

            go_down = true;
            go_up = false;

        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Right Up")) {

            go_down = false;
            go_up = true;

        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Right Down")) {

            go_down = true;
            go_up = false;

        }

        if (col.gameObject.tag == ("Red Obstacle Up") && have_Red_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Green Obstacle Up") && have_green_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Yellow Obstacle Up") && have_Yellow_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Pink Obstacle Up") && have_Pink_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Purple Obstacle Up") && have_Purple_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Brown Obstacle Up") && have_Brown_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Red Obstacle Down") && have_Red_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Green Obstacle Down") && have_green_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Yellow Obstacle Down") && have_Yellow_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Pink Obstacle Down") && have_Pink_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);

        }

        if (col.gameObject.tag == ("Purple Obstacle Down") && have_Purple_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Brown Obstacle Down") && have_Brown_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Red Obstacle Left") && have_Red_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Green Obstacle Left") && have_green_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Yellow Obstacle Left") && have_Yellow_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Pink Obstacle Left") && have_Pink_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Purple Obstacle Left") && have_Purple_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Brown Obstacle Left") && have_Brown_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Red Obstacle Right") && have_Red_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);

        }

        if (col.gameObject.tag == ("Green Obstacle Right") && have_green_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);

        }

        if (col.gameObject.tag == ("Yellow Obstacle Right") && have_Yellow_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);

        }

        if (col.gameObject.tag == ("Pink Obstacle Right") && have_Pink_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Purple Obstacle Right") && have_Purple_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);

        }

        if (col.gameObject.tag == ("Brown Obstacle Right") && have_Brown_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);

        }

        if (col.gameObject.tag == ("Red Bomb Up")) {

            RedBall ();

            go_down = false;
            go_up = true;

            have_Red_bomb = true;
            have_green_bomb = false;
            have_Yellow_bomb = false;
            have_Pink_bomb = false;
            have_Purple_bomb = false;
            have_Brown_bomb = false;
        }

        if (col.gameObject.tag == ("Red Bomb Down")) {

            RedBall ();

            go_down = true;
            go_up = false;

            have_Red_bomb = true;
            have_green_bomb = false;
            have_Yellow_bomb = false;
            have_Pink_bomb = false;
            have_Purple_bomb = false;
            have_Brown_bomb = false;
        }

        if (col.gameObject.tag == ("Green Bomb Up")) {

            GreenBall ();

            go_down = false;
            go_up = true;

            have_Red_bomb = false;
            have_green_bomb = true;
            have_Yellow_bomb = false;
            have_Pink_bomb = false;
            have_Purple_bomb = false;
            have_Brown_bomb = false;
        }

        if (col.gameObject.tag == ("Green Bomb Down")) {

            GreenBall ();

            go_down = true;
            go_up = false;

            have_Red_bomb = false;
            have_green_bomb = true;
            have_Yellow_bomb = false;
            have_Pink_bomb = false;
            have_Purple_bomb = false;
            have_Brown_bomb = false;
        }

        if (col.gameObject.tag == ("Yellow Bomb Up")) {

            YellowBall ();

            go_down = false;
            go_up = true;

            have_Red_bomb = false;
            have_green_bomb = false;
            have_Yellow_bomb = true;
            have_Pink_bomb = false;
            have_Purple_bomb = false;
            have_Brown_bomb = false;
        }

        if (col.gameObject.tag == ("Yellow Bomb Down")) {

            YellowBall ();

            go_down = true;
            go_up = false;

            have_Red_bomb = false;
            have_green_bomb = false;
            have_Yellow_bomb = true;
            have_Pink_bomb = false;
            have_Purple_bomb = false;
            have_Brown_bomb = false;
        }

        if (col.gameObject.tag == ("Pink Bomb Up")) {

            PinkBall ();

            go_down = false;
            go_up = true;

            have_Red_bomb = false;
            have_green_bomb = false;
            have_Yellow_bomb = false;
            have_Pink_bomb = true;
            have_Purple_bomb = false;
            have_Brown_bomb = false;
        }

        if (col.gameObject.tag == ("Pink Bomb Down")) {

            PinkBall ();
            go_down = true;
            go_up = false;

            have_Red_bomb = false;
            have_green_bomb = false;
            have_Yellow_bomb = false;
            have_Pink_bomb = true;
            have_Purple_bomb = false;
            have_Brown_bomb = false;
        }

        if (col.gameObject.tag == ("Purple Bomb Up")) {

            PurpleBall ();

            go_down = false;
            go_up = true;

            have_Red_bomb = false;
            have_green_bomb = false;
            have_Yellow_bomb = false;
            have_Pink_bomb = false;
            have_Purple_bomb = true;
            have_Brown_bomb = false;
        }

        if (col.gameObject.tag == ("Purple Bomb Down")) {

            PurpleBall ();

            go_down = true;
            go_up = false;

            have_Red_bomb = false;
            have_green_bomb = false;
            have_Yellow_bomb = false;
            have_Pink_bomb = false;
            have_Purple_bomb = true;
            have_Brown_bomb = false;
        }

        if (col.gameObject.tag == ("Brown Bomb Up")) {

            BrownBall ();

            go_down = false;
            go_up = true;

            have_Red_bomb = false;
            have_green_bomb = false;
            have_Yellow_bomb = false;
            have_Pink_bomb = false;
            have_Purple_bomb = false;
            have_Brown_bomb = true;
        }

        if (col.gameObject.tag == ("Brown Bomb Down")) {

            BrownBall ();

            go_down = true;
            go_up = false;

            have_Red_bomb = false;
            have_green_bomb = false;
            have_Yellow_bomb = false;
            have_Pink_bomb = false;
            have_Purple_bomb = false;
            have_Brown_bomb = true;
        }

        if (col.gameObject.tag == ("Red Obstacle Left") && have_Red_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Green Obstacle Left") && have_green_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Yellow Obstacle Left") && have_Yellow_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Pink Obstacle Left") && have_Pink_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Purple Obstacle Left") && have_Purple_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Brown Obstacle Left") && have_Brown_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Red Obstacle Right") && have_Red_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Green Obstacle Right") && have_green_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Yellow Obstacle Right") && have_Yellow_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Pink Obstacle Right") && have_Pink_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Purple Obstacle Right") && have_Purple_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Brown Obstacle Right") && have_Brown_bomb == true) {
            col.transform.parent.transform.parent.gameObject.transform.position = new Vector2 (col.transform.parent.transform.parent.gameObject.transform.position.x + 1000, col.transform.parent.transform.parent.gameObject.transform.position.x + 1000);
        }

        if (col.gameObject.tag == ("Obstacle Left")) {

            can_move_right = false;
        }

        if (col.gameObject.tag == ("Obstacle Right")) {

            can_move_left = false;
        }

        if (col.gameObject.tag == ("Green Obstacle Left")) {

            can_move_right = false;
        }

        if (col.gameObject.tag == ("Green Obstacle Right")) {

            can_move_left = false;
        }

        if (col.gameObject.tag == ("Red Obstacle Left")) {

            can_move_right = false;
        }

        if (col.gameObject.tag == ("Red Obstacle Right")) {

            can_move_left = false;
        }

        if (col.gameObject.tag == ("Yellow Obstacle Left")) {

            can_move_right = false;
        }

        if (col.gameObject.tag == ("Yellow Obstacle Right")) {

            can_move_left = false;
        }

        if (col.gameObject.tag == ("Pink Obstacle Left")) {

            can_move_right = false;
        }

        if (col.gameObject.tag == ("Pink Obstacle Right")) {

            can_move_left = false;
        }

        if (col.gameObject.tag == ("Purple Obstacle Left")) {

            can_move_right = false;
        }

        if (col.gameObject.tag == ("Purple Obstacle Right")) {

            can_move_left = false;
        }

        if (col.gameObject.tag == ("Brown Obstacle Left")) {

            can_move_right = false;
        }

        if (col.gameObject.tag == ("Brown Obstacle Right")) {

            can_move_left = false;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Up Left")) {

            can_move_right = false;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Up Right")) {

            can_move_left = false;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Down Left")) {

            can_move_right = false;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Down Right")) {

            can_move_left = false;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Right Left")) {

            can_move_right = false;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Right Right")) {

            Destroy (transform.parent.gameObject);
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Left Left")) {

            Destroy (transform.parent.gameObject);
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Left Right")) {

            can_move_left = false;
        }

        if (col.gameObject.tag == ("Green Bomb Left")) {

            GreenBall ();

            can_move_right = false;

            have_Red_bomb = false;
            have_green_bomb = true;
            have_Yellow_bomb = false;
            have_Pink_bomb = false;
            have_Purple_bomb = false;
            have_Brown_bomb = false;
        }

        if (col.gameObject.tag == ("Green Bomb Right")) {

            GreenBall ();

            can_move_left = false;

            have_Red_bomb = false;
            have_green_bomb = true;
            have_Yellow_bomb = false;
            have_Pink_bomb = false;
            have_Purple_bomb = false;
            have_Brown_bomb = false;
        }

        if (col.gameObject.tag == ("Red Bomb Left")) {

            RedBall ();

            can_move_right = false;

            have_Red_bomb = true;
            have_green_bomb = false;
            have_Yellow_bomb = false;
            have_Pink_bomb = false;
            have_Purple_bomb = false;
            have_Brown_bomb = false;
        }

        if (col.gameObject.tag == ("Red Bomb Right")) {

            RedBall ();

            can_move_left = false;

            have_Red_bomb = true;
            have_green_bomb = false;
            have_Yellow_bomb = false;
            have_Pink_bomb = false;
            have_Purple_bomb = false;
            have_Brown_bomb = false;
        }

        if (col.gameObject.tag == ("Yellow Bomb Left")) {

            YellowBall ();

            can_move_right = false;

            have_Red_bomb = false;
            have_green_bomb = false;
            have_Yellow_bomb = true;
            have_Pink_bomb = false;
            have_Purple_bomb = false;
            have_Brown_bomb = false;
        }

        if (col.gameObject.tag == ("Yellow Bomb Right")) {

            YellowBall ();

            can_move_left = false;

            have_Red_bomb = false;
            have_green_bomb = false;
            have_Yellow_bomb = true;
            have_Pink_bomb = false;
            have_Purple_bomb = false;
            have_Brown_bomb = false;
        }

        if (col.gameObject.tag == ("Pink Bomb Left")) {

            PinkBall ();

            can_move_right = false;

            have_Red_bomb = false;
            have_green_bomb = false;
            have_Yellow_bomb = false;
            have_Pink_bomb = true;
            have_Purple_bomb = false;
            have_Brown_bomb = false;
        }

        if (col.gameObject.tag == ("Pink Bomb Right")) {

            PinkBall ();

            can_move_left = false;

            have_Red_bomb = false;
            have_green_bomb = false;
            have_Yellow_bomb = false;
            have_Pink_bomb = true;
            have_Purple_bomb = false;
            have_Brown_bomb = false;
        }

        if (col.gameObject.tag == ("Purple Bomb Left")) {

            PurpleBall ();

            can_move_right = false;

            have_Red_bomb = false;
            have_green_bomb = false;
            have_Yellow_bomb = false;
            have_Pink_bomb = false;
            have_Purple_bomb = true;
            have_Brown_bomb = false;
        }

        if (col.gameObject.tag == ("Purple Bomb Right")) {

            PurpleBall ();

            can_move_left = false;

            have_Red_bomb = false;
            have_green_bomb = false;
            have_Yellow_bomb = false;
            have_Pink_bomb = false;
            have_Purple_bomb = true;
            have_Brown_bomb = false;
        }

        if (col.gameObject.tag == ("Brown Bomb Left")) {

            BrownBall ();

            can_move_right = false;

            have_Red_bomb = false;
            have_green_bomb = false;
            have_Yellow_bomb = false;
            have_Pink_bomb = false;
            have_Purple_bomb = false;
            have_Brown_bomb = true;
        }

        if (col.gameObject.tag == ("Brown Bomb Right")) {

            BrownBall ();

            can_move_left = false;

            have_Red_bomb = false;
            have_green_bomb = false;
            have_Yellow_bomb = false;
            have_Pink_bomb = false;
            have_Purple_bomb = false;
            have_Brown_bomb = true;
        }

    }

    void OnCollisionExit2D (Collision2D col) {

        if (col.gameObject.tag == ("Obstacle Left")) {

            can_move_right = true;
        }

        if (col.gameObject.tag == ("Obstacle Right")) {

            can_move_left = true;
        }

        if (col.gameObject.tag == ("Green Obstacle Left")) {

            can_move_right = true;
        }

        if (col.gameObject.tag == ("Green Obstacle Right")) {

            can_move_left = true;
        }

        if (col.gameObject.tag == ("Red Obstacle Left")) {

            can_move_right = true;
        }

        if (col.gameObject.tag == ("Red Obstacle Right")) {

            can_move_left = true;
        }

        if (col.gameObject.tag == ("Yellow Obstacle Left")) {

            can_move_right = true;
        }

        if (col.gameObject.tag == ("Yellow Obstacle Right")) {

            can_move_left = true;
        }

        if (col.gameObject.tag == ("Pink Obstacle Left")) {

            can_move_right = true;
        }

        if (col.gameObject.tag == ("Pink Obstacle Right")) {

            can_move_left = true;
        }

        if (col.gameObject.tag == ("Purple Obstacle Left")) {

            can_move_right = true;
        }

        if (col.gameObject.tag == ("Purple Obstacle Right")) {

            can_move_left = true;
        }

        if (col.gameObject.tag == ("Brown Obstacle Left")) {

            can_move_right = true;
        }

        if (col.gameObject.tag == ("Brown Obstacle Right")) {

            can_move_left = true;
        }

        if (col.gameObject.tag == ("Green Bomb Left")) {

            can_move_right = true;
        }

        if (col.gameObject.tag == ("Green Bomb Right")) {

            can_move_left = true;
        }

        if (col.gameObject.tag == ("Red Bomb Left")) {

            can_move_right = true;
        }

        if (col.gameObject.tag == ("Red Bomb Right")) {

            can_move_left = true;
        }

        if (col.gameObject.tag == ("Yellow Bomb Left")) {

            can_move_right = true;
        }

        if (col.gameObject.tag == ("Yellow Bomb Right")) {

            can_move_left = true;
        }

        if (col.gameObject.tag == ("Pink Bomb Left")) {

            can_move_right = true;
        }

        if (col.gameObject.tag == ("Pink Bomb Right")) {

            can_move_left = true;
        }

        if (col.gameObject.tag == ("Purple Bomb Left")) {

            can_move_right = true;
        }

        if (col.gameObject.tag == ("Purple Bomb Right")) {

            can_move_left = true;
        }

        if (col.gameObject.tag == ("Brown Bomb Left")) {

            can_move_right = true;
        }

        if (col.gameObject.tag == ("Brown Bomb Right")) {

            can_move_left = true;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Up Left")) {

            can_move_right = true;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Up Right")) {

            can_move_left = true;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Down Left")) {

            can_move_right = true;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Down Right")) {

            can_move_left = true;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Left Right")) {

            can_move_left = true;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Left Left")) {

            can_move_right = true;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Right Right")) {

            can_move_left = true;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Right Left")) {

            can_move_right = true;
        }
    }

    public void RedBall () {

        gameObject.GetComponent<SpriteRenderer> ().sprite = Resources.Load<Sprite> ("Ball Red");

    }

    public void GreenBall () {

        gameObject.GetComponent<SpriteRenderer> ().sprite = Resources.Load<Sprite> ("Ball Green");

    }

    public void YellowBall () {

        gameObject.GetComponent<SpriteRenderer> ().sprite = Resources.Load<Sprite> ("Ball Yellow");

    }

    public void PinkBall () {

        gameObject.GetComponent<SpriteRenderer> ().sprite = Resources.Load<Sprite> ("Ball Pink");

    }

    public void PurpleBall () {

        gameObject.GetComponent<SpriteRenderer> ().sprite = Resources.Load<Sprite> ("Ball Purple");

    }

    public void BrownBall () {

        gameObject.GetComponent<SpriteRenderer> ().sprite = Resources.Load<Sprite> ("Ball Brown");

    }
}