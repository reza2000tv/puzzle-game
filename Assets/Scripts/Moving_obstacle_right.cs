﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moving_obstacle_right : MonoBehaviour {
    public float speed;

    public bool collision = false;

    // Start is called before the first frame update
    void Start () {

        speed = 0.05f;

    }

    // Update is called once per frame
    void Update () {

        if (collision == false) {

            transform.parent.transform.parent.transform.position = new Vector2 (transform.parent.transform.parent.transform.position.x + speed, transform.parent.transform.parent.transform.position.y);

        }

    }

    void OnCollisionEnter2D (Collision2D col) {
        if (col.gameObject.tag == ("Obstacle Left")) {

            collision = true;
        }

        if (col.gameObject.tag == ("Red Obstacle Left")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Green Obstacle Left")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Yellow Obstacle Left")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Pink Obstacle Left")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Purple Obstacle Left")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Brown Obstacle Left")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Red Bomb Left")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Green Bomb Left")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Yellow Bomb Left")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Pink Bomb Left")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Purple Bomb Left")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Brown Bomb Left")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Up Left")) {

            collision = true;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Down Left")) {

            collision = true;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Left Left")) {

            collision = true;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Right Left")) {

            collision = true;
        }

    }

    void OnCollisionExit2D (Collision2D col) {

        if (col.gameObject.tag == ("Obstacle Left")) {

            collision = false;
        }

        if (col.gameObject.tag == ("Red Obstacle Left")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Green Obstacle Left")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Yellow Obstacle Left")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Pink Obstacle Left")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Purple Obstacle Left")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Brown Obstacle Left")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Red Bomb Left")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Green Bomb Left")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Yellow Bomb Left")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Pink Bomb Left")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Purple Bomb Left")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Brown Bomb Left")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Up Left")) {

            collision = false;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Down Left")) {

            collision = false;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Left Left")) {

            collision = false;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Right Left")) {

            collision = false;
        }

    }
}