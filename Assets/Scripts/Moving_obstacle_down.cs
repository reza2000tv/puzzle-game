﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moving_obstacle_down : MonoBehaviour {
    public float speed;

    public bool collision = false;

    // Start is called before the first frame update
    void Start () {

        speed = 0.05f;

    }

    // Update is called once per frame
    void Update () {

        if (collision == false) {

            transform.parent.transform.parent.transform.position = new Vector2 (transform.parent.transform.parent.transform.position.x, transform.parent.transform.parent.transform.position.y - speed);
        }

    }

    void OnCollisionStay2D (Collision2D col) {

        if (col.gameObject.tag == ("Obstacle Up")) {

            collision = true;
        }

        if (col.gameObject.tag == ("Red Obstacle Up")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Green Obstacle Up")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Yellow Obstacle Up")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Pink Obstacle Up")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Purple Obstacle Up")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Brown Obstacle Up")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Red Bomb Up")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Green Bomb Up")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Yellow Bomb Up")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Pink Bomb Up")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Purple Bomb Up")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Brown Bomb Up")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Up Up")) {

            collision = true;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Down Up")) {

            collision = true;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Left Up")) {

            collision = true;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Right Up")) {

            collision = true;
        }

    }

    void OnCollisionExit2D (Collision2D col) {

        if (col.gameObject.tag == ("Obstacle Up")) {

            collision = false;
        }

        if (col.gameObject.tag == ("Red Obstacle Up")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Green Obstacle Up")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Yellow Obstacle Up")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Pink Obstacle Up")) {

            Debug.Log ("yes");

            collision = false;

        }

        if (col.gameObject.tag == ("Purple Obstacle Up")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Brown Obstacle Up")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Red Bomb Up")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Green Bomb Up")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Yellow Bomb Up")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Pink Bomb Up")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Purple Bomb Up")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Brown Bomb Up")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Up Up")) {

            collision = false;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Down Up")) {

            collision = false;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Left Up")) {

            collision = false;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Right Up")) {

            collision = false;
        }
    }
}