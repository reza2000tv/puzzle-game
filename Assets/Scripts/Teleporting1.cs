﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporting1 : MonoBehaviour {

	public bool camera_moving_is_done;

	// Use this for initialization
	void Start () {

		camera_moving_is_done = false;

	}

	// Update is called once per frame
	void Update () {

	}

	public void OnTriggerEnter2D (Collider2D col) {
		if (col.gameObject.tag == "Player") {
			StartCoroutine (Teleport ());
		}
	}

	IEnumerator Teleport () {

		GameObject camera = GameObject.Find ("Main Camera");
		Camera_Follow camera_fallow = camera.GetComponent<Camera_Follow> ();

		GameObject Ball = GameObject.Find ("Ball");

		GameObject Portal = GameObject.Find ("Portal_out");

		Ball.SetActive (false);
		camera_fallow.Camera_Follow_Ball = false;

		yield return new WaitUntil (() => camera_fallow.Camera_Follow_Ball == true);

		Ball.SetActive (true);

		Ball.transform.position = new Vector2 (Portal.transform.position.x, Portal.transform.position.y);

	}
}