﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Follow : MonoBehaviour {
    //public Transform Ball;
    //public Transform Portal;
    public Vector3 offset;

    public bool Camera_Follow_Ball = true;
    public float speed;
    // Start is called before the first frame update
    void Start () {

        speed = 0.05f;

    }

    // Update is called once per frame
    void Update () {

        GameObject Ball = GameObject.Find ("Ball");

        GameObject Portal = GameObject.Find ("Portal_out");

        if (Camera_Follow_Ball == true) {

            transform.position = new Vector3 (Ball.transform.position.x + offset.x, Ball.transform.position.y + offset.y, -50);
        }

        if (Camera_Follow_Ball == false) {

            if (Mathf.Abs (transform.position.x - Portal.transform.position.x) >= speed) {
                if (transform.position.x > Portal.transform.position.x) {

                    transform.position = new Vector3 (transform.position.x - speed, transform.position.y, transform.position.z);
                } else {
                    transform.position = new Vector3 (transform.position.x + speed, transform.position.y, transform.position.z);
                }
            }

            if (Mathf.Abs (transform.position.y - Portal.transform.position.y) >= speed) {
                if (transform.position.y > Portal.transform.position.y) {

                    transform.position = new Vector3 (transform.position.x, transform.position.y - speed, transform.position.z);
                } else {
                    transform.position = new Vector3 (transform.position.x, transform.position.y + speed, transform.position.z);
                }
            }

            if (Mathf.Abs (transform.position.y - Portal.transform.position.y) <= speed && Mathf.Abs (transform.position.x - Portal.transform.position.x) <= speed) {

                Camera_Follow_Ball = true;
            }

        }

    }
}