﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moving_obstacle_left : MonoBehaviour {
    public float speed;

    public bool collision = false;

    // Start is called before the first frame update
    void Start () {

        speed = 0.05f;

    }

    // Update is called once per frame
    void Update () {

        if (collision == false) {

            transform.parent.transform.parent.transform.position = new Vector2 (transform.parent.transform.parent.transform.position.x - speed, transform.parent.transform.parent.transform.position.y);

        }

    }

    void OnCollisionEnter2D (Collision2D col) {
        if (col.gameObject.tag == ("Obstacle Right")) {

            collision = true;
        }

        if (col.gameObject.tag == ("Red Obstacle Right")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Green Obstacle Right")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Yellow Obstacle Right")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Pink Obstacle Right")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Purple Obstacle Right")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Brown Obstacle Right")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Red Bomb Right")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Green Bomb Right")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Yellow Bomb Right")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Pink Bomb Right")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Purple Bomb Right")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Brown Bomb Right")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Up Right")) {

            collision = true;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Down Right")) {

            collision = true;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Left Right")) {

            collision = true;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Right Right")) {

            collision = true;
        }

    }

    void OnCollisionExit2D (Collision2D col) {

        if (col.gameObject.tag == ("Obstacle Right")) {

            collision = false;
        }

        if (col.gameObject.tag == ("Red Obstacle Right")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Green Obstacle Right")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Yellow Obstacle Right")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Pink Obstacle Right")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Purple Obstacle Right")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Brown Obstacle Right")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Red Bomb Right")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Green Bomb Right")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Yellow Bomb Right")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Pink Bomb Right")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Purple Bomb Right")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Brown Bomb Right")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Up Right")) {

            collision = false;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Down Right")) {

            collision = false;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Left Right")) {

            collision = false;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Right Right")) {

            collision = false;
        }

    }
}