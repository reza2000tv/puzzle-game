﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moving_obstacle_up : MonoBehaviour {
    public float speed;

    public bool collision = false;

    // Start is called before the first frame update
    void Start () {

        speed = 0.05f;

    }

    // Update is called once per frame
    void Update () {

        if (collision == false) {

            transform.parent.transform.parent.transform.position = new Vector2 (transform.parent.transform.parent.transform.position.x, transform.parent.transform.parent.transform.position.y + speed);
        }

    }

    void OnCollisionStay2D (Collision2D col) {

        if (col.gameObject.tag == ("Obstacle Down")) {

            collision = true;
        }

        if (col.gameObject.tag == ("Red Obstacle Down")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Green Obstacle Down")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Yellow Obstacle Down")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Pink Obstacle Down")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Purple Obstacle Down")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Brown Obstacle Down")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Red Bomb Down")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Green Bomb Down")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Yellow Bomb Down")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Pink Bomb Down")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Purple Bomb Down")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Brown Bomb Down")) {

            collision = true;

        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Up Down")) {

            collision = true;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Down Down")) {

            collision = true;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Left Down")) {

            collision = true;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Right Down")) {

            collision = true;
        }

    }

    void OnCollisionExit2D (Collision2D col) {

        if (col.gameObject.tag == ("Obstacle Down")) {

            collision = false;
        }

        if (col.gameObject.tag == ("Red Obstacle Down")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Green Obstacle Down")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Yellow Obstacle Down")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Pink Obstacle Down")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Purple Obstacle Down")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Brown Obstacle Down")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Red Bomb Down")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Green Bomb Down")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Yellow Bomb Down")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Pink Bomb Down")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Purple Bomb Down")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Brown Bomb Down")) {

            collision = false;

        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Up Down")) {

            collision = false;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Down Down")) {

            collision = false;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Left Down")) {

            collision = false;
        }

        if (col.gameObject.tag == ("Moving_Obstacle_ Right Down")) {

            collision = false;
        }

    }
}